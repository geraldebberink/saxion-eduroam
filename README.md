Raspberry Pi on eduroam at Saxion
=================================

These two files are needed to get it up and running

Normally you do not have internet while doing this so just edit the
two files manually.

copy the files
```
sudo cp interfaces /etc/network/interfaces
sudo cp wpa_supplicant.conf /etc/wpa_supplicant/wpa_supplicant.conf
```

then edit /etc/wpa\_supplicant/wpa\_supplicant
```
sudo nano /etc/wpa_supplicant/wpa_supplicant.conf
```
and change your username and password
